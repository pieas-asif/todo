import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoey/models/task_data.dart';
import 'package:todoey/screens/tasks_screen.dart';
import 'package:todoey/utils/user_prefs.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserPrefs>(create: (context) => UserPrefs()),
        ChangeNotifierProvider<TaskData>(create: (context) => TaskData()),
      ],
      child: Consumer<UserPrefs>(
        builder: (context, notifier, child) {
          return MaterialApp(
            theme: Provider.of<UserPrefs>(context).isDark
                ? ThemeData.dark()
                : ThemeData.light(),
            home: TasksScreen(),
          );
        },
      ),
    );
  }
}
