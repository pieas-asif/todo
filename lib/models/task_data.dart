import 'package:flutter/foundation.dart';
import 'package:todoey/models/task.dart';
import 'dart:collection';

import 'package:todoey/utils/database_helper.dart';

class TaskData extends ChangeNotifier {
  List<Task> _tasks = [];

  void getDataFromDB() async {
    List<Map<String, dynamic>> db_data =
        await DatabaseHelper.instance.queryAll();
    _tasks.clear();
    db_data.forEach((element) {
      _tasks.add(Task(
          id: element[DatabaseHelper.id],
          name: element[DatabaseHelper.todo],
          isDone: element[DatabaseHelper.isDone] == 0 ? false : true));
    });
    notifyListeners();
  }

  UnmodifiableListView<Task> get tasks => UnmodifiableListView(_tasks);
  int get taskCount => _tasks.length;

  void addTask(String name) async {
    int id = await DatabaseHelper.instance
        .insert({DatabaseHelper.todo: name, DatabaseHelper.isDone: 0});
    getDataFromDB();
    notifyListeners();
  }

  void toggleCheckbox(int index, int isDone) async {
    int _ = await DatabaseHelper.instance
        .update({DatabaseHelper.id: index, DatabaseHelper.isDone: isDone});
    getDataFromDB();
    notifyListeners();
  }

  void deleteTask(int id) async {
    int _ = await DatabaseHelper.instance.delete(id);
    notifyListeners();
  }
}
