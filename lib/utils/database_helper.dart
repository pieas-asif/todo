import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final _dbName = "todo_data.db";
  static final _dbVersion = 1;

  static final _tableName = "todo_table";
  static final id = "_id";
  static final todo = "todo";
  static final isDone = "done";

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initiateDatabase();
    return _database;
  }

  _initiateDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, _dbName);
    return await openDatabase(path, version: _dbVersion, onCreate: _onCreate);
  }

  Future<void> _onCreate(Database db, int version) async {
    db.execute('''
        CREATE TABLE $_tableName (
          $id INTEGER PRIMARY KEY,
          $todo TEXT NOT NULL,
          $isDone TINYINT NOT NULL
        )
    ''');
  }

  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(_tableName, row);
  }

  Future<List<Map<String, dynamic>>> queryAll() async {
    Database db = await instance.database;
    return await db.query(_tableName);
  }

  Future<int> update(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int dbId = row[id];
    return await db
        .update(_tableName, row, where: '$id = ?', whereArgs: [dbId]);
  }

  Future<int> delete(int dbId) async {
    Database db = await instance.database;
    return await db.delete(_tableName, where: '$id = ?', whereArgs: [dbId]);
  }
}
