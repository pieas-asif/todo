import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPrefs extends ChangeNotifier {
  final String key = "isDark";
  static SharedPreferences _prefs;

  bool _isDark;

  bool get isDark => _isDark;

  UserPrefs() {
    _isDark = false;
    _loadFromPrefs();
  }

  void toggleTheme() {
    _isDark = !_isDark;
    _saveToPrefs();
    notifyListeners();
  }

  Future _initPrefs() async {
    if (_prefs == null) {
      _prefs = await SharedPreferences.getInstance();
    }
  }

  Future _loadFromPrefs() async {
    await _initPrefs();
    _isDark = _prefs.getBool(key) ?? false;
    notifyListeners();
  }

  Future _saveToPrefs() async {
    await _initPrefs();
    _prefs.setBool(key, _isDark);
  }
}
